import React from 'react'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import '../../css/play2d.css'

import hint from '../../images/hint.jpg'
import StopWatch from '../../components/stopwatch/stopWatch/stopWatch.js'

class Cube extends React.Component {
    constructor() {
      super();
      document.addEventListener("keydown", this.keyboard_handler, false);
      this.state = {
        // test_face : ['0', '1', '2', '3', '4', '5', '6', '7', '8'],
        top : Array(9).fill(`yellow`),
        bottom : Array(9).fill(`white`),
        right : Array(9).fill(`red`),
        left : Array(9).fill(`orange`),
        front : Array(9).fill(`blue`),
        back : Array(9).fill(`green`),
        solved : true,
        is_scrambling : true
      };
    }

    // event handlers
    keyboard_handler = (event) => {
      var rotation_dict = {//face rotations
                  'R' : this.r_prime,
                  'r' : this.r,
                  'U' : this.u_prime,
                  'u' : this.u,
                  'L' : this.l_prime,
                  'l' : this.l,
                  'F' : this.f_prime,
                  'f' : this.f,
                  'B' : this.b_prime,
                  'b' : this.b,
                  'D' : this.d_prime,
                  'd' : this.d,
                  //cube rotations
                  'Y' : this.y_prime,
                  'y' : this.y,
                  'X' : this.x_prime,
                  'x' : this.x,
                  'Z' : this.z_prime,
                  'z' : this.z
      }
      if (event.key in rotation_dict || event.key.toUpperCase() in rotation_dict) {
        rotation_dict[event.key]();
      }
    }

    scramble_change_handler = (event) => {
      this.setState({input_scramble : event.target.value});
    }

    scramble_submit_handler = () => {
      var rotation_dict = {//face rotations
        'R' : this.r_prime,
        'r' : this.r,
        'U' : this.u_prime,
        'u' : this.u,
        'L' : this.l_prime,
        'l' : this.l,
        'F' : this.f_prime,
        'f' : this.f,
        'B' : this.b_prime,
        'b' : this.b,
        'D' : this.d_prime,
        'd' : this.d,
        //cube rotations
        'Y' : this.y_prime,
        'y' : this.y,
        'X' : this.x_prime,
        'x' : this.x,
        'Z' : this.z_prime,
        'z' : this.z
      }
      let scramble = '';
      try {
        scramble = this.state.input_scramble.toLowerCase();
      }
      catch(error) {
        console.log("empty scramble provided");
      }

      this.reset();
      for (let i=0; i<scramble.length; i++) {
        if (scramble[i] in rotation_dict) {
          if (scramble[i+1] === `'`) {
            rotation_dict[scramble[i].toUpperCase()]();
            i += 1;
          }
          else if(scramble[i+1] === `2`) {
            rotation_dict[scramble[i]]();
            rotation_dict[scramble[i]]();
            i += 1;
          }
          else {
            rotation_dict[scramble[i]]();
          }
        }
      }
    }

    generate_scramble_handler = () => {
      var rotation_dict = {//face rotations
        'R' : this.r_prime,
        'r' : this.r,
        'U' : this.u_prime,
        'u' : this.u,
        'L' : this.l_prime,
        'l' : this.l,
        'F' : this.f_prime,
        'f' : this.f,
        'B' : this.b_prime,
        'b' : this.b,
        'D' : this.d_prime,
        'd' : this.d,
        //cube rotations
        'Y' : this.y_prime,
        'y' : this.y,
        'X' : this.x_prime,
        'x' : this.x,
        'Z' : this.z_prime,
        'z' : this.z
      }
      let scramble = '';
      let rotations = ['r', 'R', 'u', 'U', 'f', 'F', 'l', 'L', 'd', 'D', 'b', 'B'];

      // this.reset();
      for (let i=0; i<21; i++) {
        let rand_move = rotations[Math.floor(Math.random() * rotations.length)]
        scramble = scramble + rand_move + ' ';
        rotation_dict[rand_move]();
      }
      console.log(scramble);
    }

    check_cube_hander = () => {
      // check each face of the cube
      let faces = [this.state.top , this.state.bottom , this.state.right , this.state.left , this.state.front , this.state.back];
      for (let i=0; i<faces.length; i++) {
        if (this.check_cube_face(faces[i]) === false) {
          return false;
        }
      }
      // TODO: stop timer and send time/username to database
      return true;
    }

    check_cube_face(face) {
      let color = face[0];
      for (let i=1; i<face.length; i++) {
        if (color !== face[i]) {
          return false;
        }
      }
      return true;
    }

    toggle_scramble_mode = () => {
      this.setState({is_scrambling : this.state.is_scrambling === false});
    }

    // face rotation functions
    r = () => {
      // rotate colors on the right face
      let new_right = this.state.right;
      rotateFaceCW(new_right);
      this.setState({right : new_right});

      // rotate colors along the right face
      let new_top = this.state.top;
      let new_back = this.state.back;
      let new_bottom = this.state.bottom;
      let new_front = this.state.front;

      let faces = [new_top, new_back, new_bottom, new_front];
      let columns = [[new_top[2], new_top[5], new_top[8]], [new_back[6], new_back[3], new_back[0]], [new_bottom[2], new_bottom[5], new_bottom[8]], [new_front[2], new_front[5], new_front[8]]];   //note: back values mirrored

      shiftRight(...[0, 1, 2, 3], columns);
      for (let u=0; u<faces.length; u++) {
        if (faces[u] === new_back) {
          faces[u][6] = columns[u][0];    //note: back values mirrored
          faces[u][3] = columns[u][1];
          faces[u][0] = columns[u][2];
        }
        else {
          faces[u][2] = columns[u][0];
          faces[u][5] = columns[u][1];
          faces[u][8] = columns[u][2];
        }
      }

      this.setState({top : [...new_top]});
      this.setState({back : [...new_back]});
      this.setState({bottom : [...new_bottom]});
      this.setState({front : [...new_front]});
      this.setState({solved : this.check_cube_hander()});
    }

    r_prime = () => {
      // rotate colors on the right face
      let new_right = this.state.right;
      rotateFaceCCW(new_right);
      this.setState({right : new_right});

      // rotate colors along the right face
      let new_top = this.state.top;
      let new_back = this.state.back;
      let new_bottom = this.state.bottom;
      let new_front = this.state.front;

      let faces = [new_top, new_front, new_bottom, new_back];
      let columns = [[new_top[2], new_top[5], new_top[8]], [new_front[2], new_front[5], new_front[8]], [new_bottom[2], new_bottom[5], new_bottom[8]], [new_back[6], new_back[3], new_back[0]]];

      shiftRight(...[0, 1, 2, 3], columns);
      for (let u=0; u<faces.length; u++) {
        if (faces[u] === new_back) {
          faces[u][6] = columns[u][0];    //note: back values mirrored
          faces[u][3] = columns[u][1];
          faces[u][0] = columns[u][2];
        }
        else {
          faces[u][2] = columns[u][0];
          faces[u][5] = columns[u][1];
          faces[u][8] = columns[u][2];
        }
      }

      this.setState({top : [...new_top]});
      this.setState({back : [...new_back]});
      this.setState({bottom : [...new_bottom]});
      this.setState({front : [...new_front]});
      this.setState({solved : this.check_cube_hander()});
    }

    u = () => {
      // rotate colors on top face
      let new_top = this.state.top;
      rotateFaceCW(new_top);
      this.setState({top : [...new_top]});
    
      // rotate colors along the top face
      let new_front = this.state.front;
      let new_left = this.state.left;
      let new_back = this.state.back;
      let new_right = this.state.right;

      let faces = [new_front, new_left, new_back, new_right];
      let rows = [[new_front[0], new_front[1], new_front[2]], [new_left[0], new_left[1], new_left[2]], [new_back[0], new_back[1], new_back[2]], [new_right[0], new_right[1], new_right[2]]];
    
      shiftRight(...[0, 1, 2, 3], rows);
      for (let u=0; u<faces.length; u++) {
        faces[u][0] = rows[u][0];
        faces[u][1] = rows[u][1];
        faces[u][2] = rows[u][2];
      }

      this.setState({front : [...new_front]});
      this.setState({left : [...new_left]});
      this.setState({back : [...new_back]});
      this.setState({right : [...new_right]});
      this.setState({solved : this.check_cube_hander()});
    }
    
    u_prime = () => {
      // rotate colors on top face
      let new_top = this.state.top;
      rotateFaceCCW(new_top);
      this.setState({top : [...new_top]});
    
      // rotate colors along the top face
      let new_front = this.state.front;
      let new_left = this.state.left;
      let new_back = this.state.back;
      let new_right = this.state.right;

      let faces = [new_right, new_back, new_left, new_front];
      let rows = [[new_right[0], new_right[1], new_right[2]], [new_back[0], new_back[1], new_back[2]], [new_left[0], new_left[1], new_left[2]], [new_front[0], new_front[1], new_front[2]]];

      shiftRight(...[0, 1, 2, 3], rows);
      for (let u=0; u<faces.length; u++) {
        faces[u][0] = rows[u][0];
        faces[u][1] = rows[u][1];
        faces[u][2] = rows[u][2];
      }

      this.setState({front : [...new_front]});
      this.setState({left : [...new_left]});
      this.setState({back : [...new_back]});
      this.setState({right : [...new_right]});
      this.setState({solved : this.check_cube_hander()});
    }

    f = () => {
      // rotate colors on front face
      let new_front = this.state.front;
      rotateFaceCW(new_front);
      this.setState({front : [...new_front]});
    
      // rotate colors along the front face
      let new_top = this.state.top;
      let new_right = this.state.right;
      let new_bottom = this.state.bottom;
      let new_left = this.state.left;

      let rows =[[new_top[6], new_top[7], new_top[8]], [new_right[0], new_right[3], new_right[6]], [new_bottom[2], new_bottom[1], new_bottom[0]], [new_left[8], new_left[5], new_left[2]]];
      shiftRight(...[0, 1, 2, 3], rows);

      new_top[6] = rows[0][0]
      new_top[7] = rows[0][1];
      new_top[8] = rows[0][2];
      new_right[0] = rows[1][0]
      new_right[3] = rows[1][1];
      new_right[6] = rows[1][2];
      new_bottom[2] = rows[2][0]
      new_bottom[1] = rows[2][1];
      new_bottom[0] = rows[2][2];
      new_left[8] = rows[3][0]
      new_left[5] = rows[3][1];
      new_left[2] = rows[3][2];

      this.setState({top : [...new_top]});
      this.setState({right : [...new_right]});
      this.setState({bottom : [...new_bottom]});
      this.setState({left : [...new_left]});
      this.setState({solved : this.check_cube_hander()});
    }
    
    f_prime = () => {
      // rotate colors on front face
      let new_front = this.state.front;
      rotateFaceCCW(new_front);
      this.setState({front : [...new_front]});
    
      // rotate colors along the front face
      let new_top = this.state.top;
      let new_right = this.state.right;
      let new_bottom = this.state.bottom;
      let new_left = this.state.left;

      let rows = [[new_bottom[2], new_bottom[1], new_bottom[0]], [new_left[8], new_left[5], new_left[2]], [new_top[6], new_top[7], new_top[8]], [new_right[0], new_right[3], new_right[6]]];
      shiftRight(...[0, 1, 2, 3], rows);

      new_top[6] = rows[0][0]
      new_top[7] = rows[0][1];
      new_top[8] = rows[0][2];
      new_right[0] = rows[1][0]
      new_right[3] = rows[1][1];
      new_right[6] = rows[1][2];
      new_bottom[2] = rows[2][0]
      new_bottom[1] = rows[2][1];
      new_bottom[0] = rows[2][2];
      new_left[8] = rows[3][0]
      new_left[5] = rows[3][1];
      new_left[2] = rows[3][2];

      this.setState({top : [...new_top]});
      this.setState({right : [...new_right]});
      this.setState({bottom : [...new_bottom]});
      this.setState({left : [...new_left]});
      this.setState({solved : this.check_cube_hander()});
    }

    l = () => {
      // rotate colors on left face
      let new_left = this.state.left;
      rotateFaceCW(new_left);
      this.setState({left : [...new_left]});
    
      // rotate colors along the left face
      let new_front = this.state.front;
      let new_bottom = this.state.bottom;
      let new_back = this.state.back;
      let new_top = this.state.top;

      let faces = [new_front, new_bottom, new_back, new_top];
      let columns = [[new_front[0], new_front[3], new_front[6]], [new_bottom[0], new_bottom[3], new_bottom[6]], [new_back[8], new_back[5], new_back[2]], [new_top[0], new_top[3], new_top[6]]];

      shiftRight(...[0, 1, 2, 3], columns);
      for (let i=0; i<faces.length; i++) {
          if (faces[i] === new_back) {
              faces[i][8] = columns[i][0];
              faces[i][5] = columns[i][1];
              faces[i][2] = columns[i][2];
          }
          else {
              faces[i][0] = columns[i][0];
              faces[i][3] = columns[i][1];
              faces[i][6] = columns[i][2];
          }
      }
      this.setState({solved : this.check_cube_hander()});
    }
    
    l_prime = () => {
      // rotate colors on left face
      let new_left = this.state.left;
      rotateFaceCCW(new_left);
      this.setState({left : [...new_left]});
    
      // rotate colors along the left face
      let new_front = this.state.front;
      let new_bottom = this.state.bottom;
      let new_back = this.state.back;
      let new_top = this.state.top;

      let faces = [new_top, new_back, new_bottom, new_front];
      let columns = [[new_top[0], new_top[3], new_top[6]], [new_back[8], new_back[5], new_back[2]], [new_bottom[0], new_bottom[3], new_bottom[6]], [new_front[0], new_front[3], new_front[6]]];

      shiftRight(...[0, 1, 2, 3], columns);
      for (let i=0; i<faces.length; i++) {
          if (faces[i] === new_back) {
              faces[i][8] = columns[i][0];
              faces[i][5] = columns[i][1];
              faces[i][2] = columns[i][2];
          }
          else {
              faces[i][0] = columns[i][0];
              faces[i][3] = columns[i][1];
              faces[i][6] = columns[i][2];
          }
      }
      this.setState({solved : this.check_cube_hander()});
    }
    
    d = () => {
      // rotate colors on the bottom face
      let new_bottom = this.state.bottom;
      rotateFaceCW(new_bottom);
      this.setState({bottom : [...new_bottom]});
    
      // rotate colors along the bottom face
      let new_front = this.state.front;
      let new_left = this.state.left;
      let new_back = this.state.back;
      let new_right = this.state.right;

      let faces = [new_front, new_right, new_back, new_left];
      let rows = [[new_front[6], new_front[7], new_front[8]], [new_right[6], new_right[7], new_right[8]], [new_back[6], new_back[7], new_back[8]], [new_left[6], new_left[7], new_left[8]]];

      shiftRight(...[0, 1, 2, 3], rows);
      for (let i=0; i<faces.length; i++) {
          faces[i][6] = rows[i][0];
          faces[i][7] = rows[i][1];
          faces[i][8] = rows[i][2];
      }
      this.setState({solved : this.check_cube_hander()});
    }
    
    d_prime = () => {
      // rotate colors on the bottom face
      let new_bottom = this.state.bottom;
      rotateFaceCCW(new_bottom);
      this.setState({bottom : [...new_bottom]});
    
      // rotate colors along the bottom face
      let new_front = this.state.front;
      let new_left = this.state.left;
      let new_back = this.state.back;
      let new_right = this.state.right;

      let faces = [new_left, new_back, new_right, new_front];
      let rows = [[new_left[6], new_left[7], new_left[8]], [new_back[6], new_back[7], new_back[8]], [new_right[6], new_right[7], new_right[8]], [new_front[6], new_front[7], new_front[8]]];

      shiftRight(...[0, 1, 2, 3], rows);
      for (let i=0; i<faces.length; i++) {
          faces[i][6] = rows[i][0];
          faces[i][7] = rows[i][1];
          faces[i][8] = rows[i][2];
      }
      this.setState({solved : this.check_cube_hander()});
    }

    b = () => {
      // rotate colors on the back face
      let new_back = this.state.back;
      rotateFaceCW(new_back);
      this.setState({back : new_back});
      
      this.setState({back : [...new_back]});
      // rotate colors along the back face
      let new_top = this.state.top;
      let new_right = this.state.right;
      let new_bottom = this.state.bottom;
      let new_left = this.state.left;

      let rows =[[new_top[0], new_top[1], new_top[2]], [new_left[0], new_left[3], new_left[6]], [new_bottom[6], new_bottom[7], new_bottom[8]], [new_right[2], new_right[5], new_right[8]]];

      shiftRight(...[0, 1, 2, 3], rows);
      new_top[0] = rows[0][0];
      new_top[1] = rows[0][1];
      new_top[2] = rows[0][2];
      new_left[0] = rows[1][2];
      new_left[3] = rows[1][1];
      new_left[6] = rows[1][0];
      new_right[2] = rows[3][2];
      new_right[5] = rows[3][1];
      new_right[8] = rows[3][0];
      new_bottom[6] = rows[2][0];
      new_bottom[7] = rows[2][1];
      new_bottom[8] = rows[2][2];

      this.setState({top : [...new_top]});
      this.setState({right : [...new_right]});
      this.setState({bottom : [...new_bottom]});
      this.setState({left : [...new_left]});
      this.setState({solved : this.check_cube_hander()});
    }
    
    b_prime = () => {
      // rotate colors on the back face
      let new_back = this.state.back;
      rotateFaceCCW(new_back);
      this.setState({back : [...new_back]});

      // rotate colors along the back face
      let new_top = this.state.top;
      let new_right = this.state.right;
      let new_bottom = this.state.bottom;
      let new_left = this.state.left;

      let rows = [[new_bottom[6], new_bottom[7], new_bottom[8]], [new_right[2], new_right[5], new_right[8]], [new_top[0], new_top[1], new_top[2]], [new_left[6], new_left[3], new_left[0]]];

      shiftRight(...[0, 1, 2, 3], rows);
      new_top[0] = rows[0][0];
      new_top[1] = rows[0][1];
      new_top[2] = rows[0][2];
      new_left[0] = rows[1][0];
      new_left[3] = rows[1][1];
      new_left[6] = rows[1][2];
      new_right[2] = rows[3][0];
      new_right[5] = rows[3][1];
      new_right[8] = rows[3][2];
      new_bottom[6] = rows[2][2];
      new_bottom[7] = rows[2][1];
      new_bottom[8] = rows[2][0];

      this.setState({top : [...new_top]});
      this.setState({right : [...new_right]});
      this.setState({bottom : [...new_bottom]});
      this.setState({left : [...new_left]});
      this.setState({solved : this.check_cube_hander()});
    }    

    // cube rotation functions
    y = () => {
      // rotate the cube faces
      let original_front = this.state.front;
      this.setState({front : this.state.right});
      this.setState({right : this.state.back});
      this.setState({back : this.state.left});
      this.setState({left : original_front});

      // rotate colors on top and bottom faces
      let new_top = this.state.top;
      let new_bottom =  this.state.bottom;
      
      rotateFaceCW(new_top);
      rotateFaceCCW(new_bottom);
      this.setState({top : [...new_top]});
      this.setState({bottom : [...new_bottom]});
    }

    y_prime = () => {
      // rotate the cube faces
      let original_front = this.state.front;
      this.setState({front : this.state.left});
      this.setState({left : this.state.back});
      this.setState({back : this.state.right});
      this.setState({right : original_front});

      // rotate colors on top and bottom faces
      let new_top = this.state.top;
      let new_bottom =  this.state.bottom;
      
      rotateFaceCCW(new_top);
      rotateFaceCW(new_bottom);
      this.setState({top : [...new_top]});
      this.setState({bottom : [...new_bottom]});
    }

    x = () => {
      // mirror faces going into and out the back
      let new_back = this.state.back;
      let new_top = this.state.top;
      reverse(new_back);
      reverse(new_top);

      // rotate the cube faces
      this.setState({top : this.state.front});
      this.setState({front : this.state.bottom});
      this.setState({bottom : new_back});
      this.setState({back : new_top});

      // rotate colors on left and right faces
      let new_right = this.state.right;
      let new_left = this.state.left;
      rotateFaceCW(new_right);
      rotateFaceCCW(new_left);

      this.setState({left : [...new_left]});
      this.setState({right : [...new_right]});
    }

    x_prime = () => {
      // mirror faces going into and out the back
      let new_back = this.state.back;
      let new_bottom = this.state.bottom;
      reverse(new_back);
      reverse(new_bottom);

      // rotate the cube faces
      let original_top = this.state.top;
      this.setState({top : new_back});
      this.setState({back : new_bottom});
      this.setState({bottom : this.state.front});
      this.setState({front : original_top});

      // rotate colors on left and right faces
      let new_right = this.state.right;
      let new_left = this.state.left;
      rotateFaceCW(new_left);
      rotateFaceCCW(new_right);

      this.setState({left : [...new_left]});
      this.setState({right : [...new_right]});
    }

    z = () => {
      let original_right = this.state.right;
      let new_top = this.state.top;
      let new_left = this.state.left;
      let new_bottom = this.state.bottom;

      // rotate faces to accomodate new orientation
      rotateFaceCW(original_right);
      rotateFaceCW(new_top);
      rotateFaceCW(new_left);
      rotateFaceCW(new_bottom);

      // shift faces
      this.setState({right : new_top});
      this.setState({top : new_left});
      this.setState({left : new_bottom});
      this.setState({bottom : original_right});

      // rotate colors on the front and back faces
      let new_front = this.state.front;
      let new_back = this.state.back;
      rotateFaceCW(new_front);
      rotateFaceCCW(new_back);

      this.setState({front : new_front});
      this.setState({back : new_back});
    }

    z_prime = () => {
      let original_right = this.state.right;
      let new_top = this.state.top;
      let new_left = this.state.left;
      let new_bottom = this.state.bottom;

      // rotate faces to accomodate new orientation
      rotateFaceCCW(original_right);
      rotateFaceCCW(new_top);
      rotateFaceCCW(new_left);
      rotateFaceCCW(new_bottom);

      // shift faces
      this.setState({right : new_bottom});
      this.setState({bottom : new_left});
      this.setState({left : new_top});
      this.setState({top : original_right});

      // rotate colors on the front and back faces
      let new_front = this.state.front;
      let new_back = this.state.back;
      rotateFaceCW(new_back);
      rotateFaceCCW(new_front);

      this.setState({front : new_front});
      this.setState({back : new_back});
    }

    // special cube functions
    reset = () => {
      this.setState({top : Array(9).fill(`yellow`)});
      this.setState({bottom : Array(9).fill(`white`)});
      this.setState({right : Array(9).fill(`red`)});
      this.setState({left : Array(9).fill(`orange`)});
      this.setState({front : Array(9).fill(`blue`)});
      this.setState({back : Array(9).fill(`green`)});
    }

    render() {
      return (
        <div className='play_container'>
          <h3>Paste in a scramble here:</h3>
          <input id="scramble_input" type="text" value={this.state.value} onChange={this.scramble_change_handler}/>
          <button id="scramble_button" onClick={this.scramble_submit_handler}>Scramble!</button>
          <button id="make_scramble_button" onClick={this.generate_scramble_handler}>Generate a random scramble!</button>
          <button id="toggle_scramble_mode" onClick={this.toggle_scramble_mode} style={{backgroundColor: this.state.is_scrambling ? 'green' : 'red'}}>
            Scramble mode: {this.state.is_scrambling ? 'ON' : 'OFF'}
          </button>
          <StopWatch is_solved={this.state.solved} is_scrambling={this.state.is_scrambling}/>
          <Container fluid>
            <Row>
              <Col>
              <section id="THE-CUBE">
                  <section className="side" id="top-side">
                    <button id="top0" style={{backgroundColor: this.state.top[0]}}></button>
                    <button id="top1" style={{backgroundColor: this.state.top[1]}}></button>
                    <button id="top2" style={{backgroundColor: this.state.top[2]}}></button>
                    <button id="top3" style={{backgroundColor: this.state.top[3]}}></button>
                    <button id="top4" style={{backgroundColor: this.state.top[4]}}></button>
                    <button id="top5" style={{backgroundColor: this.state.top[5]}}></button>
                    <button id="top6" style={{backgroundColor: this.state.top[6]}}></button>
                    <button id="top7" style={{backgroundColor: this.state.top[7]}}></button>
                    <button id="top8" style={{backgroundColor: this.state.top[8]}}></button>
                  </section>

                  <section className="side" id="left-side">
                    <button id="left0" style={{backgroundColor: this.state.left[0]}}></button>
                    <button id="left1" style={{backgroundColor: this.state.left[1]}}></button>
                    <button id="left2" style={{backgroundColor: this.state.left[2]}}></button> 
                    <button id="left3" style={{backgroundColor: this.state.left[3]}}></button>
                    <button id="left4" style={{backgroundColor: this.state.left[4]}}></button>
                    <button id="left5" style={{backgroundColor: this.state.left[5]}}></button>
                    <button id="left6" style={{backgroundColor: this.state.left[6]}}></button>
                    <button id="left7" style={{backgroundColor: this.state.left[7]}}></button>
                    <button id="left8" style={{backgroundColor: this.state.left[8]}}></button>
                  </section>

                  <section className="side" id="front-side">
                    <button id="front0" style={{backgroundColor: this.state.front[0]}}></button>
                    <button id="front1" style={{backgroundColor: this.state.front[1]}}></button>
                    <button id="front2" style={{backgroundColor: this.state.front[2]}}></button>
                    <button id="front3" style={{backgroundColor: this.state.front[3]}}></button>
                    <button id="front4" style={{backgroundColor: this.state.front[4]}}></button>
                    <button id="front5" style={{backgroundColor: this.state.front[5]}}></button>
                    <button id="front6" style={{backgroundColor: this.state.front[6]}}></button>
                    <button id="front7" style={{backgroundColor: this.state.front[7]}}></button>
                    <button id="front8" style={{backgroundColor: this.state.front[8]}}></button>
                  </section>
                  
                  <section className="side" id="right-side">
                    <button id="right0" style={{backgroundColor: this.state.right[0]}}></button>
                    <button id="right1" style={{backgroundColor: this.state.right[1]}}></button>
                    <button id="right2" style={{backgroundColor: this.state.right[2]}}></button>
                    <button id="right3" style={{backgroundColor: this.state.right[3]}}></button>
                    <button id="right4" style={{backgroundColor: this.state.right[4]}}></button>
                    <button id="right5" style={{backgroundColor: this.state.right[5]}}></button>
                    <button id="right6" style={{backgroundColor: this.state.right[6]}}></button>
                    <button id="right7" style={{backgroundColor: this.state.right[7]}}></button>
                    <button id="right8" style={{backgroundColor: this.state.right[8]}}></button>
                  </section>

                  <section className="side" id="bottom-side">
                    <button id="bottom0" style={{backgroundColor: this.state.bottom[0]}}></button>
                    <button id="bottom1" style={{backgroundColor: this.state.bottom[1]}}></button>
                    <button id="bottom2" style={{backgroundColor: this.state.bottom[2]}}></button>
                    <button id="bottom3" style={{backgroundColor: this.state.bottom[3]}}></button>
                    <button id="bottom4" style={{backgroundColor: this.state.bottom[4]}}></button>
                    <button id="bottom5" style={{backgroundColor: this.state.bottom[5]}}></button>
                    <button id="bottom6" style={{backgroundColor: this.state.bottom[6]}}></button>
                    <button id="bottom7" style={{backgroundColor: this.state.bottom[7]}}></button>
                    <button id="bottom8" style={{backgroundColor: this.state.bottom[8]}}></button>
                  </section>

                  <section className="side" id="back-side">
                    <button id="back0" style={{backgroundColor: this.state.back[2]}}></button>
                    <button id="back1" style={{backgroundColor: this.state.back[1]}}></button>
                    <button id="back2" style={{backgroundColor: this.state.back[0]}}></button>
                    <button id="back3" style={{backgroundColor: this.state.back[5]}}></button>
                    <button id="back4" style={{backgroundColor: this.state.back[4]}}></button>
                    <button id="back5" style={{backgroundColor: this.state.back[3]}}></button>
                    <button id="back6" style={{backgroundColor: this.state.back[8]}}></button>
                    <button id="back7" style={{backgroundColor: this.state.back[7]}}></button>
                    <button id="back8" style={{backgroundColor: this.state.back[6]}}></button>
                  </section>
              </section>
              </Col>
              <Col>
            <h1>Rotations</h1>
            <Row>
              <Col>right face:</Col>
              <Col>
                <Move letter="R" func={this.r}/>
                <Move letter="R'" func={this.r_prime}/>
              </Col>
            </Row>
            <Row>
              <Col>top face:</Col>
              <Col>              
                <Move letter="U" func={this.u}/>
                <Move letter="U'" func={this.u_prime}/>
              </Col>
            </Row>
            <Row>
              <Col>left face:</Col>
              <Col>
                <Move letter="L" func={this.l}/>
                <Move letter="L'" func={this.l_prime}/>
              </Col>
            </Row>
            <Row>
              <Col>front face:</Col>
              <Col>
                <Move letter="F" func={this.f}/>
                <Move letter="F'" func={this.f_prime}/>
              </Col>
            </Row>
            <Row>
              <Col>back face:</Col>
              <Col>
                <Move letter="B" func={this.b}/>
                <Move letter="B'" func={this.b_prime}/>
              </Col>
            </Row>
            <Row>
              <Col>bottom face:</Col>
              <Col>
                <Move letter="D" func={this.d}/>
                <Move letter="D'" func={this.d_prime}/>
              </Col>
            </Row>  
            <Row>
              <Col>reposition vertically:</Col>
              <Col>
                <Move letter="x" func={this.x}/>
                <Move letter="x'" func={this.x_prime}/>
              </Col>
            </Row>
            <Row>
              <Col>reposition horizontally:</Col>
              <Col>
                <Move letter="y" func={this.y}/>
                <Move letter="y'" func={this.y_prime}/>
              </Col>
            </Row>
            <Row>
              <Col>twist cube:</Col>
              <Col>
                <Move letter="z" func={this.z}/>
                <Move letter="z'" func={this.z_prime}/>
              </Col>
            </Row>                      
            {/* <button id="check" onClick={this.check_cube_hander}>Check Cube</button> */}
              </Col>
            </Row>
            <Row>
              <Col className="hint">
                <br></br> <br></br>
                <div id="scamble_mode_explanation">
                  <h2> How To Use the Timer</h2>
                  <p> Scramble mode should be ON when you want to scramble the cube without starting the timer. After scrambling the cube, turn scramble mode OFF to start the timer
                      (the timer will stop automatically once the cube is solved).
                  </p>
                </div>
                <h2> Controls </h2>
                <p>You can use the buttons or your keyboard to move the cube. Lowercase letters will rotate the appropriate face
                  clockwise and uppercase letters will rotate the face counterclockwise (ex. pressing 'r' on the keyboard will rotate the right face clockwise, 
                  but pressing shift + 'r' will rotate the right face counterclockwise).
                </p>
              </Col>
              <Col className="hint">
                <img
                  id="hints"
                  src={hint}
                  alt="face rotations"
                />
              </Col>
            </Row>
          </Container>
        </div>
      );
    }
};

export default Cube;

function shiftRight(x, y, z, t, array) {
  let temp = array[t];
    let list = [x, y, z, t];
    for (let i=2; i>=0; i--) {
      array[list[i+1]] = array[list[i]];
    }
    array[list[0]] = temp;
}

// function to rotate face colors clockwise (9-element array represents a 3x3 grid)
function rotateFaceCW(array) {
  // rotate corners
  shiftRight(...[2, 8, 6, 0], array);
  // rotate edges
  shiftRight(...[5, 7, 3, 1], array);
}

// function to rotate face colors counterclockwise (9-element array represents a 3x3 grid)
function rotateFaceCCW(array) {
  // rotate corners
  shiftRight(...[0, 6, 8, 2], array);
  // rotate edges
  shiftRight(...[1, 3, 7, 5], array);
}

function reverse(array) {
  let arrayLength = array.length;
    for (let i=0; i <= Math.floor(arrayLength / 2); i++) {
        let temp = array[i];
        array[i] = array[arrayLength-1-i];
        array[arrayLength-1-i] = temp;
    }
}

function Move({letter, func}) {
  function handleClick() {
    func();
  }
  return <button onClick={handleClick}>{letter}</button>
}
