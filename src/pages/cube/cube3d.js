import  { useEffect } from 'react';
import * as THREE from 'three';

function ThreeDCanvas() {
    useEffect(() => {
        // Set up the scene
    alert("3d Cube Is IN progress, the cube is not fully functional!")
    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(75, 1000 / 800, 0.1, 1000); // Initial dimensions of 1000x800
    camera.position.z = 8;

    // Create the renderer
    const renderer = new THREE.WebGLRenderer({ alpha: true }); // Enable alpha for transparent background
    renderer.setSize(600, 450); // Initial dimensions of 1000x800
    renderer.setClearAlpha(0); // Set clear alpha to fully transparent
    const canvas = renderer.domElement;
    canvas.style.background = 'linear-gradient(to bottom, #87CEEB, #1E90FF)';
    canvas.style.border = 'solid 5px rgb(120, 0, 0)'; // Add box shadow border
    canvas.style.margin = 'auto'; // Center horizontally
    // Don't append the canvas here
    document.body.appendChild(canvas);
        // Define the dimensions of the Rubik's Cube
        const cubeSize = 3;
        const cubeSpacing = 0.05;
        const pieceSize = 1;

        // Define colors for each face of the Rubik's Cube
        const colors = [
            0xEC3223, // Red
            0x6BE447, // Green
            0x0022F5, // Blue
            0xFFFD53, // Yellow
            0xF2A83B, // Orange
            0xffffff, // White
        ];

        // Function to create a single Rubik's Cube piece
        function createCubePiece() {
            const geometry = new THREE.BoxGeometry(pieceSize, pieceSize, pieceSize);
            const materials = [
                new THREE.MeshBasicMaterial({ color: colors[0] }), // Front (Red)
                new THREE.MeshBasicMaterial({ color: colors[4] }), // Back (Orange)
                new THREE.MeshBasicMaterial({ color: colors[5] }), // Top (white)
                new THREE.MeshBasicMaterial({ color: colors[3] }), // Bottom (Yellow)
                new THREE.MeshBasicMaterial({ color: colors[1] }), // Right (Green)
                new THREE.MeshBasicMaterial({ color: colors[2] }), // Left (Blue)
            ];
            return new THREE.Mesh(geometry, materials);
        }

        // Create the Rubik's Cube pieces and group them by face
        const rubiksCube = {
            front: [],
            back: [],
            top: [],
            bottom: [],
            right: [],
            left: []
        };

        for (let x = 0; x < cubeSize; x++) {
            for (let y = 0; y < cubeSize; y++) {
                for (let z = 0; z < cubeSize; z++) {
                    const cubePiece = createCubePiece();
                    cubePiece.position.set(
                        (x - cubeSize / 2) * (pieceSize + cubeSpacing),
                        (y - cubeSize / 2) * (pieceSize + cubeSpacing),
                        (z - cubeSize / 2) * (pieceSize + cubeSpacing)
                    );
                    scene.add(cubePiece);

                    if (x === 0) rubiksCube.left.push(cubePiece);
                    if (x === cubeSize - 1) rubiksCube.right.push(cubePiece);
                    if (y === 0) rubiksCube.bottom.push(cubePiece);
                    if (y === cubeSize - 1) rubiksCube.top.push(cubePiece);
                    if (z === 0) rubiksCube.front.push(cubePiece);
                    if (z === cubeSize - 1) rubiksCube.back.push(cubePiece);
                }
            }
        }

        // Function to rotate a face of the Rubik's Cube
        function rotateFace(face, angle) {
            const axis = face === 'front' || face === 'back' ? 'z' : (face === 'top' || face === 'bottom' ? 'y' : 'x');
            const cubes = rubiksCube[face];
            const pivot = new THREE.Vector3();
            cubes.forEach(cube => pivot.add(cube.position));
            pivot.divideScalar(cubes.length);

            // Save original positions
            const originalPositions = cubes.map(cube => cube.position.clone());

            // Rotate each cube piece around the pivot
            cubes.forEach(cube => {
                cube.position.sub(pivot);
                cube.position.applyAxisAngle(new THREE.Vector3().copy(axis === 'x' ? new THREE.Vector3(1, 0, 0) : (axis === 'y' ? new THREE.Vector3(0, 1, 0) : new THREE.Vector3(0, 0, 1))), angle);
                cube.position.add(pivot);
                cube.rotateOnWorldAxis(new THREE.Vector3().copy(axis === 'x' ? new THREE.Vector3(1, 0, 0) : (axis === 'y' ? new THREE.Vector3(0, 1, 0) : new THREE.Vector3(0, 0, 1))), angle);
            });

            // Update cube positions
            for (let i = 0; i < cubes.length; i++) {
                const cube = cubes[i];
                cube.position.copy(originalPositions[i]);
            }
        }

        // Define variables for mouse rotation
        let isDragging = false;
        let previousMousePosition = {
            x: 0,
            y: 0
        };

        // Event listeners for mouse controls
        document.addEventListener('mousedown', event => {
            isDragging = true;
            previousMousePosition = {
                x: event.clientX,
                y: event.clientY
            };
        });

        document.addEventListener('mousemove', event => {
            if (isDragging) {
                const deltaMove = {
                    x: -1 * (event.clientX - previousMousePosition.x),
                    y: -1 * (event.clientY - previousMousePosition.y)
                };

                // Rotate the cube based on mouse movement
                const sensitivity = 0.005; // Adjust the sensitivity of rotation
                scene.rotation.x -= deltaMove.y * sensitivity;
                scene.rotation.y -= deltaMove.x * sensitivity;

                previousMousePosition = {
                    x: event.clientX,
                    y: event.clientY
                };
            }
        });

        document.addEventListener('mouseup', () => {
            isDragging = false;
        });

        // Event listener for keyboard controls
        document.addEventListener('keydown', event => {
            switch (event.key) {
                case 'r':
                    rotateFace('right', Math.PI / 2);
                    break;
                case 'l':
                    rotateFace('left', -Math.PI / 2);
                    break;
                case 'u':
                    rotateFace('top', Math.PI / 2);
                    break;
                case 'd':
                    rotateFace('bottom', -Math.PI / 2);
                    break;
                case 'f':
                    rotateFace('front', Math.PI / 2);
                    break;
                case 'b':
                    rotateFace('back', -Math.PI / 2);
                    break;
                default:
                    break;
            }
        });

        // Render the scene
        function animate() {
            requestAnimationFrame(animate);
            renderer.render(scene, camera);
        }
        animate();

        // Define the cleanup function
        return () => {
            // Remove the canvas from the document body
            document.body.removeChild(canvas);
        };
    }, []);

    return null; // No need to return any JSX since this component is only responsible for side effects
}

export default ThreeDCanvas;
