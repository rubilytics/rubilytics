import React, { useEffect, useState } from 'react';
import '../css/play.css';
import ThreeDCanvas from './cube/cube3d';

function Play() {
  // eslint-disable-next-line
  const [canvasRendered, setCanvasRendered] = useState(false);

  useEffect(() => {
    setCanvasRendered(true); // Set canvasRendered to true when component mounts
  }, []); // Empty dependency array ensures this runs only once

  return (
    <>
      <div className='play_container'>
        <div className='space_2d'></div>
        <div className="leg_contain">
          <div id="legend">
            <h2>Legend</h2>
            <ul>
              <li><strong>Mouse Drag:</strong> Rotate the cube</li>
              <li><strong>Arrow Keys:</strong> Rotate cube faces (r: right, l: left, u: up, d: down, f: front, b: back)</li>
            </ul>
          </div>
        </div>
      </div>
      <ThreeDCanvas /> {/* Render the ThreeDCanvas component directly */}
    </>
  );
}

export default Play;
