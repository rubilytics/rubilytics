import React, { useEffect } from 'react';
import '../css/rankings.css';
import Table from 'react-bootstrap/Table';

function Rankings() {
  useEffect(() => {
    alert("This page is still in development. Coming soon!");
  }, []); // Empty dependency array ensures this effect runs only once on mount

  return (
    <div className='rankings_container'>
      <div className='ranking_title_section'>
        <h1>Rubilytics</h1>
      </div>
      <Table bordered size="md" className='rankings_table'>
        <thead>
          <tr>
            <th>Ranking Number</th>
            <th>Username</th>
            <th>Time</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>1</td>
            <td>ExampleUser</td>
            <td>20.24 s</td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
}

export default Rankings;
