import React from 'react';
import { useNavigate } from 'react-router-dom'; // Use useNavigate instead of useHistory
import '../css/rules.css';
import rbo from '../images/rubo.png';
import rbt from '../images/rubt.png';

function Rules() {
  const navigate = useNavigate(); // Use useNavigate instead of useHistory

  const handleButtonClick = () => {
    navigate('/SiteRules'); // Use navigate instead of history.push
  };
  return (
    <>
    <div className="anim_contain">
      <img id='one_cube' src={rbo} alt="" />
      <div className="rules_contain">
        <h1 className="rules_sub"><b>How To Play</b></h1>
        <div className="rules_text">
          <p className="text_one">Welcome to the world of Rubik's Cube solving! If you're new to the cube or
            looking to improve your solving skills, follow these step-by-step instructions:</p>
          <ol>
            <li>Understand the Basics
              <ul>
                <li>Cube Anatomy: Get familiar with the different parts of the Rubik's Cube:
                  the six faces, each consisting of nine smaller squares of a specific color.
                  Notation: Learn the standard notation used for describing cube
                  movements. Common notations include F (Front), B (Back), U (Up), D (Down),
                  L (Left), and R (Right). Movements can be clockwise (e.g., R) or counterclockwise (e.g., R').</li>
              </ul>
            </li>
            <li>Solve the First Layer
              <ul>
                <li>
                  Start with a Cross: Begin by solving the cross on one face. Align the edges with the center color of that face.
                  Complete the First Layer: Fill in the remaining squares on the first layer to match the center color of each face.</li>
              </ul>
            </li>
            <li>Solve the Second Layer
              <ul>
                <li>
                  Align Edge Pieces: Focus on solving the second layer by pairing edge pieces with the corresponding center colors.
                </li>
              </ul>
            </li>
            <li> Solve the Final Layer
              <ul>
                <li>
                  Orient the Last Layer: Rotate the last layer's pieces to match the center color. This step is often called "OLL" (Orientation of the Last Layer).
                  Permute the Last Layer: Arrange the last layer's pieces into their correct positions. This step is referred to as "PLL" (Permutation of the Last Layer).
                </li>
              </ul>
            </li>
            <li> Practice and Speedcubing
              <ul>
                <li>
                  Memorize Algorithms: As you progress, memorize algorithms to solve specific patterns efficiently.
                  Improve Your Time: Challenge yourself to solve the cube faster by refining your techniques and minimizing move counts.
                </li>
              </ul>
            </li>
            <li>Additional Resources
              <ul>
                <li>
                  Online Tutorials: Explore online tutorials and videos for additional tips and advanced solving methods.
                  Join the Community: Connect with fellow cubers in online communities to share experiences and strategies.
                </li>
              </ul>
            </li>
          </ol>
          <p>Remember, solving the Rubik's Cube takes practice and patience. Don't be discouraged if it takes time to master the techniques. Enjoy the journey of becoming a Rubik's Cube solver!</p>
          <button id="rules_btn" onClick={handleButtonClick}>Site Rules</button>

        </div>

      </div>
      <img src={rbt} alt="" />
      </div>
    </>
  )
}

export default Rules