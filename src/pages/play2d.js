import React from 'react'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import '../css/play2d.css'
import Cube from './cube/2dCube.js'

function play() {
  return (
    <div className='play_container'>
      <Container fluid>
        <Row>
          <Col>
            <section className='play_title_section'></section>
          </Col>
        </Row>
        <Row>
          <Col>
            <Cube/>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default play