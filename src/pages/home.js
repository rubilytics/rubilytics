import React from 'react';
import Carousel from 'react-bootstrap/Carousel';
import { useNavigate } from 'react-router-dom'; // Use useNavigate instead of useHistory
import slide_o from '../images/slideone.png';
import slide_t from '../images/slidetwo.png';
import slide_thr from '../images/slidethree.png';
import slide_fo from '../images/slidefour.png';
import '../css/home.css';
import two_bg from '../images/cube.mp4';

function Home() {
  const navigate = useNavigate(); // Use useNavigate instead of useHistory

  const handleSolveButtonClick = () => {
    navigate('/Play'); // Use navigate instead of history.push
  };

  return (
    <>
      <div className="home_container">
        <div className='row_one'>
        <Carousel data-bs-theme="dark">
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={slide_o}
                alt="First slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={slide_t}
                alt="Second slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={slide_thr}
                alt="Third slide"
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={slide_fo}
                alt="Third slide"
              />
            </Carousel.Item>
          </Carousel>
        </div>
        <div className='row_two'>
          <p><b>How fast can you solve?</b></p>
          <button id="solve_btn" onClick={handleSolveButtonClick}>Solve Now</button>
          <video src={two_bg} loop autoPlay muted />
        </div>
      </div>
    </>
  );
}

export default Home;
