// Import necessary dependencies and assets
import React from 'react';
import '../css/rules.css';
import { useNavigate } from 'react-router-dom'; // Use useNavigate instead of useHistory

import rbo from '../images/rubo.png';
import rbt from '../images/rubt.png';

// Define the Rules component
function Rules() {
  const navigate = useNavigate(); // Use useNavigate instead of useHistory

  const handleButtonClick = () => {
    navigate('/Rules'); // Use navigate instead of history.push
  };
  return (
    <>
      <div className="anim_contain">
        <img id="one_cube" src={rbo} alt="" />
        <div className="rules_contain">
          <h1 className="rules_sub">
            <b>Site Rules</b>
          </h1>
          <div className="rules_text">
            <p>
              Welcome to our virtual Rubik's Cube platform! To ensure a fair and enjoyable experience for all users, we've established the following rules:
            </p>

            <ol>
              <li>
                <b>Fair Play and Sportsmanship</b>
                <ul>
                  <li>Respect other users and play fairly.</li>
                  <li>Avoid any form of cheating, including the use of external tools or algorithms.</li>
                  <li>Be a good sport, whether you win or lose.</li>
                </ul>
              </li>

              <li>
                <b>Prohibited Content</b>
                <ul>
                  <li>Do not share or promote any offensive, harmful, or inappropriate content.</li>
                  <li>Respect intellectual property rights; do not share copyrighted material without permission.</li>
                </ul>
              </li>

              <li>
                <b>User Conduct</b>
                <ul>
                  <li>Treat fellow cubers with respect and courtesy.</li>
                  <li>Refrain from engaging in any form of harassment, bullying, or discrimination.</li>
                </ul>
              </li>
              <li>
                <b>Account Usage</b>
                <ul>
                  <li>
                    Each user is responsible for their account activity. Do not share your account credentials.
                  </li>
                  <li>
                    Report any suspicious activity or unauthorized access immediately.
                  </li>
                </ul>
              </li>
              <li>
                <b>Bug Reporting</b>
                <ul>
                  <li>
                    If you encounter any bugs or glitches, report them to the platform administrators promptly.
                  </li>
                  <li>
                    Do not exploit or abuse any bugs for personal gain.
                  </li>
                </ul>
              </li>
              <li>
                <b>Community Guidelines</b>
                <ul>
                  <li>
                    Follow additional community guidelines provided by the platform.
                  </li>
                  <li>
                    Participate in a positive and constructive manner within the community.
                  </li>
                </ul>
              </li>
              <li>
                <b>Moderation</b>
                <ul>
                  <li>
                    Platform administrators reserve the right to moderate content and take appropriate actions against violators.
                  </li>
                  <li>
                    Penalties may include warnings, temporary suspensions, or permanent bans.
                  </li>
                </ul>
              </li>
              <li>
                <b>Privacy</b>
                <ul>
                  <li>
                    Respect the privacy of others. Do not share personal information without consent.
                  </li>
                  <li>
                    Familiarize yourself with the platform's privacy policy.
                  </li>
                </ul>
              </li>
              <li>
                <b>Updates to Rules</b>
                <ul>
                  <li>
                    The platform may update these rules periodically. Stay informed about any changes.
                  </li>
                </ul>
              </li>

            </ol>

            <p>
              By using our virtual Rubik's Cube platform, you agree to abide by these rules.
              Failure to comply may result in disciplinary actions, including the termination of your account.
              Thank you for contributing to a positive and enjoyable community!
            </p>

            <button id="rules_btn" onClick={handleButtonClick}>How To Play</button>
          </div>
        </div>
        <img src={rbt} alt="" />
      </div>
    </>
  );
}

// Export the Rules component
export default Rules;
