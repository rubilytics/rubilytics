import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Routes, Route } from "react-router-dom";

import App from './App';
//pages
import Home from './pages/home';
import Rules from './pages/rules';
import Play from './pages/play';
import Play2D from './pages/play2d'
import Rankings from './pages/rankings';
import SiteR from './pages/rulestwo';

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App/>}>
          <Route index element={<Home />} />
          <Route path="Home" element={<Home />} />
          <Route path="Rules" element={<Rules />} />
          <Route path="SiteRules" element={<SiteR />} />
          <Route path="Play" element={<Play />} />
          <Route path="Play2D" element={<Play2D />} />
          <Route path="Rankings" element={<Rankings />} />
        </Route>
      </Routes>
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);
