// The cube
var top = Array(9).fill(`Yellow`);
var bottom = Array(9).fill(`White`);
var right = Array(9).fill(`Red`);
var left = Array(9).fill(`Orange`);
var front = Array(9).fill(`Blue`);
var back = Array(9).fill(`Green`);

// override default to display face colors in a 3x3 grid
let faces = [front, top, back, bottom, right, left];
for (let f=0; f<6; f++) {
    faces[f].toString = function() {
        return `[ ${faces[f][0]}, ${faces[f][1]}, ${faces[f][2]},\n  ${faces[f][3]}, ${faces[f][4]}, ${faces[f][5]},\n  ${faces[f][6]}, ${faces[f][7]}, ${faces[f][8]} ]`;
    }
}

// cube notation: https://jperm.net/3x3/moves
// function to rotate the cube, given a string of rotations
function rotateCube(rotations) {
    var dict = {// face rotations
                [`R`] : R,
                ['U'] : U,
                ['L'] : L,
                ['F'] : F,
                ['B'] : B,
                ['D'] : D,
                // cube rotations
                ['y'] : y,
                ['x'] : x,
                ['z'] : z
        };
    for (let i=0; i<rotations.length; i++) {
        let prime = (rotations[i+1] == `'`);

        if (rotations[i] in dict) {
            dict[rotations[i]](prime);
        }
    }
}

// functions to rotate the cube faces
function R (prime = false) {
    let edges = Array(4);
    let corners = Array(4);
    let faces = Array(4);
    let columns = Array(4);

    if (prime) {
        // rotate colors on the right face
        edges = [3, 7, 5, 1];
        corners = [6, 8, 2, 0];

        // rotate colors along the right face
        faces = [top, front, bottom, back];
        columns = [[top[2], top[5], top[8]], [front[2], front[5], front[8]], [bottom[2], bottom[5], bottom[8]], [back[6], back[3], back[0]]];   //note: back values mirrored
    }
    else {
        // rotate colors on the right face
        edges = [1, 5, 7, 3];
        corners = [0, 2, 8, 6];
        
        // rotate colors along the right side
        faces = [top, back, bottom, front];
        columns = [[top[2], top[5], top[8]], [back[6], back[3], back[0]], [bottom[2], bottom[5], bottom[8]], [front[2], front[5], front[8]]];   //note: back values mirrored
    }
    shiftRight(...edges, right);
    shiftRight(...corners, right);
    shiftRight(...[0, 1, 2, 3], columns);
    
    for (let u=0; u<faces.length; u++) {
        if (faces[u] == back) {
            faces[u][6] = columns[u][0];    //note: back values mirrored
            faces[u][3] = columns[u][1];
            faces[u][0] = columns[u][2];
        }
        else {
            faces[u][2] = columns[u][0];
            faces[u][5] = columns[u][1];
            faces[u][8] = columns[u][2];
        }
    }
}

function U (prime = false) {
    let edges = Array(4);
    let corners = Array(4);
    let faces = Array(4);
    let rows = Array(4);

    if (prime) {
        // rotate colors on the top face
        edges = [3, 7, 5, 1];
        corners = [6, 8, 2, 0];

        // rotate colors along the top face
        faces = [right, back, left, front];
        rows = [[right[0], right[1], right[2]], [back[0], back[1], back[2]], [left[0], left[1], left[2]], [front[0], front[1], front[2]]];
    }
    else {
        // rotate colors on the top face
        edges = [1, 5, 7, 3];
        corners = [0, 2, 8, 6];
        
        // rotate colors along the top face
        faces = [front, left, back, right];
        rows = [[front[0], front[1], front[2]], [left[0], left[1], left[2]], [back[0], back[1], back[2]], [right[0], right[1], right[2]]];
    }
    shiftRight(...edges, top);
    shiftRight(...corners, top);
    shiftRight(...[0, 1, 2, 3], rows);
    for (let u=0; u<faces.length; u++) {
        faces[u][0] = rows[u][0];
        faces[u][1] = rows[u][1];
        faces[u][2] = rows[u][2];
    }
}

function L (prime = false) {
    let edges = Array(4);
    let corners = Array(4);
    let faces = Array(4);
    let columns = Array(4);

    if (prime) {
        // rotate colors on the left face
        edges = [3, 7, 5, 1];
        corners = [6, 8, 2, 0];

        faces = [top, back, bottom, front];
        columns = [[top[0], top[3], top[6]], [back[8], back[5], back[2]], [bottom[0], bottom[3], bottom[6]], [front[0], front[3], front[6]]];
    }
    else {
        // rotate colors on left face
        edges = [1, 5, 7, 3];
        corners = [0, 2, 8, 6];

        // rotate colors along left face
        faces = [front, bottom, back, top]
        columns = [[front[0], front[3], front[6]], [bottom[0], bottom[3], bottom[6]], [back[8], back[5], back[2]], [top[0], top[3], top[6]]];

    }
    shiftRight(...edges, left);
    shiftRight(...corners, left);
    shiftRight(...[0, 1, 2, 3], columns);

    for (let i=0; i<faces.length; i++) {
        if (faces[i] == back) {
            faces[i][8] = columns[i][0];
            faces[i][5] = columns[i][1];
            faces[i][2] = columns[i][2];
        }
        else {
            faces[i][0] = columns[i][0];
            faces[i][3] = columns[i][1];
            faces[i][6] = columns[i][2];
        }
    }
}

function F (prime = false) {
    let edges = Array(4);
    let corners = Array(4);
    let rows = Array(4);

    if (prime) {
        // rotate colors on the front
        edges = [3, 7, 5, 1];
        corners = [6, 8, 2, 0];

        // rotate colors along the front
        rows = [[bottom[2], bottom[1], bottom[0]], [left[8], left[5], left[2]], [top[6], top[7], top[8]], [right[0], right[3], right[6]]];
    }
    else {
        // rotate colors on the front
        edges = [1, 5, 7, 3];
        corners = [0, 2, 8, 6];

        // rotate colors along the front
        rows =[[top[6], top[7], top[8]], [right[0], right[3], right[6]], [bottom[2], bottom[1], bottom[0]], [left[8], left[5], left[2]]];
    }
    shiftRight(...edges, front);
    shiftRight(...corners, front);
    shiftRight(...[0, 1, 2, 3], rows);

    top[6] = rows[0][0]
    top[7] = rows[0][1];
    top[8] = rows[0][2];
    right[0] = rows[1][0]
    right[3] = rows[1][1];
    right[6] = rows[1][2];
    bottom[2] = rows[2][0]
    bottom[1] = rows[2][1];
    bottom[0] = rows[2][2];
    left[8] = rows[3][0]
    left[5] = rows[3][1];
    left[2] = rows[3][2];
}

function D (prime = false) {
    let edges = Array(4);
    let corners = Array(4);
    let faces = Array(4);
    let rows = Array(4);

    if (prime) {
        edges = [3, 7, 5, 1];
        corners = [6, 8, 2, 0];
        
        faces = [left, back, right, front];
        rows = [[left[6], left[7], left[8]], [back[6], back[7], back[8]], [right[6], right[7], right[8]], [front[6], front[7], front[8]]];
    }
    else {
        edges = [1, 5, 7, 3];
        corners = [0, 2, 8, 6];

        faces = [front, right, back, left];
        rows = [[front[6], front[7], front[8]], [right[6], right[7], right[8]], [back[6], back[7], back[8]], [left[6], left[7], left[8]]];
    }
    shiftRight(...edges, bottom);
    shiftRight(...corners, bottom);
    shiftRight(...[0, 1, 2, 3], rows);

    for (let i=0; i<faces.length; i++) {
        faces[i][6] = rows[i][0];
        faces[i][7] = rows[i][1];
        faces[i][8] = rows[i][2];
    }
}

function B (prime = false) {
    let edges = Array(4);
    let corners = Array(4);
    let rows = Array(4);

    if (prime) {
        edges = [3, 7, 5, 1];
        corners = [6, 8, 2, 0];
        rows = [[bottom[6], bottom[7], bottom[8]], [right[2], right[5], right[8]], [top[0], top[1], top[2]], [left[6], left[3], left[0]]];
        shiftRight(...[0, 1, 2, 3], rows);

        left[0] = rows[1][0];
        left[3] = rows[1][1];
        left[6] = rows[1][2];
        right[2] = rows[3][0];
        right[5] = rows[3][1];
        right[8] = rows[3][2];
        bottom[6] = rows[2][2];
        bottom[7] = rows[2][1];
        bottom[8] = rows[2][0];
    }
    else {
        edges = [1, 5, 7, 3];
        corners = [0, 2, 8, 6];
        rows =[[top[0], top[1], top[2]], [left[0], left[3], left[6]], [bottom[6], bottom[7], bottom[8]], [right[2], right[5], right[8]]];
        shiftRight(...[0, 1, 2, 3], rows);

        left[0] = rows[1][2];
        left[3] = rows[1][1];
        left[6] = rows[1][0];
        right[2] = rows[3][2];
        right[5] = rows[3][1];
        right[8] = rows[3][0];
        bottom[6] = rows[2][0];
        bottom[7] = rows[2][1];
        bottom[8] = rows[2][2];
    }
    shiftRight(...edges, back);
    shiftRight(...corners, back);

    top[0] = rows[0][0];
    top[1] = rows[0][1];
    top[2] = rows[0][2];
}

// functions to rotate the cube
function y (prime = false) {
    if (prime) {
        // shift faces to the right
        let originalFront = front;
        front = left;
        left = back;
        back = right;
        right = originalFront;

        // rotate colors on the top and bottom
        let edges = [3, 7, 5, 1];
        let corners = [6, 8, 2, 0];
        shiftRight(...edges, top);
        shiftRight(...corners, top);

        edges = [1, 5, 7, 3];
        corners = [0, 2, 8, 6];
        shiftRight(...edges, bottom);
        shiftRight(...corners, bottom);
    }
    else {
        // shift faces to the left
        let originalFront = front;
        front = right;
        right = back;
        back = left;
        left = originalFront;

        // rotate colors on the top and bottom
        let edges = [1, 5, 7, 3];
        let corners = [0, 2, 8, 6];
        shiftRight(...edges, top);
        shiftRight(...corners, top);

        edges = [3, 7, 5, 1];
        corners = [6, 8, 2, 0];
        shiftRight(...edges, bottom);
        shiftRight(...corners, bottom);
    }
}

function x (prime = false) {
    if (prime) {
        // shift faces up
        let originalTop = top;
        top = back;
        back = bottom;
        bottom = front;
        front = originalTop;

        // reverse faces oing into and out the back
        reverse(back);
        reverse(top);

        // rotate colors on the right and left faces
        let edges = [3, 7, 5, 1];
        let corners = [6, 8, 2, 0];
        shiftRight(...edges, right);
        shiftRight(...corners, right);

        edges = [1, 5, 7, 3];
        corners = [0, 2, 8, 6];
        shiftRight(...edges, left);
        shiftRight(...corners, left);
    }
    else {
        // shift faces up
        let originalTop = top;
        top = front;
        front = bottom;
        bottom = back;
        back = originalTop;

        // reverse faces going into and out the back
        reverse(back);
        reverse(bottom);

        // rotate colors on the right and left faces
        let edges = [1, 5, 7, 3];
        let corners = [0, 2, 8, 6];
        shiftRight(...edges, right);
        shiftRight(...corners, right);

        edges = [3, 7, 5, 1];
        corners = [6, 8, 2, 0];
        shiftRight(...edges, left);
        shiftRight(...corners, left);
    }
}

function z (prime = false) {
    let originalRight = right;
    if (prime) {
        // shift faces
        right = bottom;
        bottom = left;
        left = top;
        top = originalRight;

        // rotate faces to accomodate new orientation
        rotateFaceCCW(right);
        rotateFaceCCW(top);
        rotateFaceCCW(left);
        rotateFaceCCW(bottom);

        // rotate colors on the front and back faces
        let edges = [3, 7, 5, 1];
        let corners = [6, 8, 2, 0];
        shiftRight(...edges, front);
        shiftRight(...corners, front);

        edges = [1, 5, 7, 3];
        corners = [0, 2, 8, 6];
        shiftRight(...edges, back);
        shiftRight(...corners, back);
    }
    else {
        // shift faces
        right = top;
        top = left;
        left = bottom;
        bottom = originalRight;

        // rotate faces to accomodate new orientation
        rotateFaceCW(right);
        rotateFaceCW(top);
        rotateFaceCW(left);
        rotateFaceCW(bottom);

        // rotate colors on the front and back faces
        let edges = [1, 5, 7, 3];
        let corners = [0, 2, 8, 6];
        shiftRight(...edges, front);
        shiftRight(...corners, front);

        edges = [3, 7, 5, 1];
        corners = [6, 8, 2, 0];
        shiftRight(...edges, back);
        shiftRight(...corners, back);
    }
}

// function to shift 4 elements in an array to the right, given the indexes and the array
function shiftRight(x, y, z, t, array) {
    let temp = array[t];
    let list = [x, y, z, t];
    for (let i=2; i>=0; i--) {
        array[list[i+1]] = array[list[i]];
    }
    array[list[0]] = temp;
}

// function to reverse an array
function reverse(array) {
    let arrayLength = array.length;
    for (let i=0; i <= Math.floor(arrayLength / 2); i++) {
        let temp = array[i];
        array[i] = array[arrayLength-1-i];
        array[arrayLength-1-i] = temp;
    }
}

// function to rotate face colors clockwise (9-element array represents a 3x3 grid)
function rotateFaceCW(array) {
    // rotate corners
    shiftRight(...[2, 8, 6, 0], array);
    // rotate edges
    shiftRight(...[5, 7, 3, 1], array);
}

// function to rotate face colors counterclockwise (9-element array represents a 3x3 grid)
function rotateFaceCCW(array) {
    // rotate corners
    shiftRight(...[0, 6, 8, 2], array);
    // rotate edges
    shiftRight(...[1, 3, 7, 5], array);
}

// function to display cube face to terminal
function printFace(face) {
  let array;
    if (face == `right`) {
        array = right;
    }
    else if (face == `left`) {
        array = left;
    }
    else if (face == `top`) {
        array = top;
    }
    else if (face == 'bottom') {
        array = bottom;
    }
    else if (face == `front`) {
        array = front;
    }
    else if (face == `back`) {
        array = back;
    }
    console.log("" + array);
}

rotateCube(`R U F B L D z'`);

console.log(`top`);
printFace(`top`);
console.log(`bottom`);
printFace(`bottom`);
console.log(`front`);
printFace(`front`);
console.log(`back`);
printFace(`back`);
console.log(`right`);
printFace(`right`);
console.log(`left`);
printFace(`left`);