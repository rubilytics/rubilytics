import React from 'react';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import '../css/nav.css';
import logo from '../images/logo.png';
import { NavLink, Outlet, useLocation} from 'react-router-dom';

function CollapsibleExample() {
  const location = useLocation();
  return (
    <>
      <Navbar collapseOnSelect expand="lg" className="bg-body-tertiary nav_main" sticky="top">
        <Container>
          <Navbar.Brand as={NavLink} to="/Home">
            <img src={logo} alt="Rubilytics" className='nav_logo' />
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={NavLink} to="/Home"  className={location.pathname === '/Home' || location.pathname === '/'  ? 'active-link ' : 'nav-link'}>Home</Nav.Link>
              <Nav.Link as={NavLink} to="/Rules"  className={location.pathname === '/Rules' || location.pathname === '/SiteRules'? 'active-link': 'nav-link'}>Rules</Nav.Link>
              <Nav.Link as={NavLink} to="/Play" className={location.pathname === '/Play' ? 'active-link ' : 'nav-link'}>Play</Nav.Link>
              <Nav.Link as={NavLink} to="/Play2D" className={location.pathname === '/Play2D' ? 'active-link ' : 'nav-link'}>Play2D</Nav.Link>
              <Nav.Link as={NavLink} to="/Rankings" className={location.pathname === '/Rankings' ? 'active-link ' : 'nav-link'}>Rankings</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Outlet />
    </>
  );
}

export default CollapsibleExample;
