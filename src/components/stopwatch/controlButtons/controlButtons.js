import React from "react";
import "./controlButtons.css";
 
export default function ControlButtons(props) {
    const ActiveButtons = (
        <div className="btn-grp">
            <div className="btn btn-two"
                onClick={props.handleReset}
                hidden={!props.isPaused}>
                Reset
            </div>
        </div>
    );
 
    return (
        <div className="Control-Buttons">
            <div>{ActiveButtons}</div>
        </div>
    );
}