import React, { useState  } from "react";
import "./stopWatch.css";
import Timer from "../timer/timer.js";
import ControlButtons from "../controlButtons/controlButtons.js";
 
function StopWatch(props) {
    const [isActive, setIsActive] = useState(false);
    const [isPaused, setIsPaused] = useState(true);
    const [time, setTime] = useState(0);

    React.useEffect(() => {
        let interval = null;

        if (isActive && isPaused === false) {
            interval = setInterval(() => {
                setTime((time) => time + 10);
            }, 10);
        } else {
            clearInterval(interval);
        }

        // if the cube is solved when the timer is running, stop the timer
        if (props.is_solved && !isPaused) {
            handlePauseResume();
        }
        // start the timer automatically when scramble mode is NOT on
        if (!props.is_scrambling && !props.is_solved) {
            if (!isActive) {
                handleStart();
            }
        }

        return () => {
            clearInterval(interval);
        };
    }, [isActive, isPaused, props.is_solved, props.is_scrambling]);
 
    const handleStart = () => {
        setIsActive(true);
        setIsPaused(false);
      };
        
 
    const handlePauseResume = () => {
        setIsPaused((prevPaused) => !prevPaused);
    };
 
    const handleReset = () => {
        setIsActive(false);
        setIsPaused(true);

        setTime(0);
    };
 
    // useEffect(() => {
    //     onStart({ handleStart, handlePauseResume, handleReset });
    //   }, [onStart, handleStart, handlePauseResume, handleReset])


    return (
        <div className="stop-watch">
            <Timer time={time} />
            <ControlButtons
                active={isActive}
                isPaused={isPaused}
                handleStart={handleStart}
                handlePauseResume={handlePauseResume}
                handleReset={handleReset}
            />
        </div>
    );
}
 
export default StopWatch;
