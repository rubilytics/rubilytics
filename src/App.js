import NavBar from './components/navbar'
import Footer from './components/footer'
import './css/variables.css';
function App() {
  return (
    <>
    <div className="notice">
      <p>Switch to a Bigger device to view</p>
    </div>
      <NavBar />
      <Footer />
    </>

  );
}

export default App;
