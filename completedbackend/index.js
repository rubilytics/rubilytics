const express = require("express");
const mongoose = require("mongoose");
const app = express();


const bodyParser = require("body-parser");

const connectDB = async () => {
  try {
    const conn = await mongoose.connect('mongodb://127.0.0.1:27017/rubilytics', {

    });
    console.log(`MongoDB Connected: {conn.connection.host}`);
  } catch (error) {
    console.error(error.message);
    process.exit(1);
  }
}
connectDB();

var multer = require('multer');
var upload = multer();


// for parsing application/json
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// for parsing application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true })); 

// for parsing multipart/form-data
app.use(upload.array()); 
app.use(express.static('public'));


const Schema = mongoose.Schema;

const rubiksSchema = new Schema({
    playerPID: String,
    bestScore: mongoose.Decimal128,
    averageScore: mongoose.Decimal128,
    totalPlays: Number,
    currentScore: mongoose.Decimal128,
});
 
const playerstatsModel = mongoose.model("PlayerStats", rubiksSchema);


app.get("/",
    function (req, res) {
        res.send("Player Stats");
    });
 ``
app.post("/newUser", async function (req, res) {
        console.log("new user request received");
        const user = new playerstatsModel({
            playerPID: req.body.pid,
            bestScore: req.body.bestScore,
            averageScore: req.body.averageScore,
            totalPlays: req.body.totalplays,
            currentScore: req.body.currentscore,
        });
        console.log(typeof req.body.pid);
        console.log(user);

        checkUser = await playerstatsModel.exists({ playerPID: req.body.pid });
        console.log(checkUser) 
        if(checkUser == null) {
            results = await user.save();
            console.log(results);
            res.send(req.body.pid + " added")
        } else { // user already exists
            res.send("this account already exists in rubilytics!")
        }
  
    });


app.post("/newScore", async function (req, res) {
        console.log("new score received for a player");
        const query = playerstatsModel.where({ playerPID: req.body.pid });
        const user = await query.findOne();

        console.log(user)
        if (user != null) {


            newnumber = (user.averageScore * user.totalPlays)
            console.log(newnumber)
            newnumber = Number(newnumber) + Number(req.body.currentScore)
            console.log(newnumber)

            user.totalPlays = Number(user.totalPlays) + Number(1)

            newnumber = newnumber / user.totalPlays
            console.log(newnumber)
            user.averageScore = newnumber


            if (user.bestScore > req.body.currentScore) {
                user.bestScore = req.body.currentScore;
            }
    
            results = await user.save();
    
            console.log(results);
            res.send("scores updated")
        } else {
            res.send("user doesn't exist")
        }

    });
    

app.post("/deleteUser", async function (req, res) {
        output = await playerstatsModel.deleteOne({ playerPID: req.body.pid });
        console.log(output)
        res.send(output.deletedCount + " deleted")
    });



app.get("/rankingsTopAverage", async function (req, res) {
        console.log("return top players in json format by average score");
        output = await playerstatsModel.find().sort({ averageScore: 1 });
        console.log(output)

        res.json(output)
    });

app.get("/rankingsTopBest", async function (req, res) {
        console.log("return top players in json format by best score");
        output = await playerstatsModel.find().sort({ bestScore: 1 });
        console.log(output)

        res.json(output)
    });

app.post("/getSpecificUser", async function (req, res) {
        console.log("retrieving specific user");
        const query = playerstatsModel.where({ playerPID: req.body.pid });
        const user = await query.findOne();

        console.log(user)
        if (user != null) {
            res.json(user)
        } else {
            res.send("user doesn't exist")
        }
    });

app.listen(3001, () => {
    console.log(`Server Started at ${3001}`)
})





