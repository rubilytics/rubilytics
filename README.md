<p align="center" width="100%">
    <img width="90%" src="src/images/slideone.png"> 
</p>

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm install`
### `npm run start`

## Figma Link:

https://www.figma.com/file/BEf0hvzxvjzOz8uzUNQiOg/Project-2?type=design&node-id=0%3A1&mode=design&t=lr3t0AfCtXOYusGS-1

## Deployed Site

(https://rubilytics.netlify.app)

